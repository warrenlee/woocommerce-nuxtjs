export const actions = {
  async nuxtServerInit({ dispatch }) {
    await dispatch('cart/getCart')
    await dispatch('category/getCategories')
  },
  clearAll({ dispatch }) {
    dispatch('stripe/clearAll')
    dispatch('cart/clearCart')
  },
}
