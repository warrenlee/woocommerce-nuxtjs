export const state = () => ({
  token: '',
})

export const mutations = {
  UPDATE_NONCE(state, nonce) {
    state.token = nonce
  },
}
