export default {
  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: 'woocommerce-nuxtjs',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: ['~/assets/scss/_v-tooltip.scss'],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    '~/plugins/wp-api.js',
    '~/plugins/vue-secure-html.client.js',
    '~/plugins/vuejs-paginate.client.js',
    '~/plugins/v-tooltip.client.js',
    '~/plugins/notification.client.js',
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/stylelint
    '@nuxtjs/stylelint-module',
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    '@nuxtjs/toast',
    'nuxtjs-mdi-font',
    'nuxt-stripe-module',
    'nuxt-cookie-control',
  ],

  // nuxt-cookie-control
  cookies: {
    necessary: [
      {
        name: 'Default Cookies',
        description: 'Used for cookie control.',
        cookies: ['cookie_control_consent', 'cookie_control_enabled_cookies'],
      },
    ],
    optional: [
      {
        name: 'Google Analytics',
        identifier: 'ga',
        description: 'Google GTM is...',
        initialState: true,
        cookies: ['_ga', '_gat', '_gid'],
      },
    ],
  },

  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {
    proxy: true,
  },

  proxy: {
    '/wp-json/': process.env.BASE_URL,
  },

  stripe: {
    publishableKey: process.env.STRIPE_PK,
  },

  toast: {
    position: 'top-right',
    iconPack: 'mdi',
    singleton: true,
  },

  publicRuntimeConfig: {
    browserBaseURL: process.env.BROWSER_BASE_URL || 'http://localhost:3000',
    baseURL: process.env.BASE_URL || 'http://localhost:8080',
    perPage: process.env.PRODUCTS_PER_PAGE || 12,
    woocommerceKey: process.env.WOOCOMMERCE_KEY,
    woocommerceSecret: process.env.WOOCOMMERCE_SECRET,
    stripePublishKey: process.env.STRIPE_PK,
    paypalClientId: process.env.PAYPAL_CLIENT_ID,
  },

  privateRuntimeConfig: {},

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {},

  router: {
    extendRoutes(routes, resolve) {
      routes.push({
        name: 'home',
        path: '/',
        component: resolve(__dirname, 'pages/page/_page.vue'),
        alias: '/page/1',
      })
    },
  },
}
