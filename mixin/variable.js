import { get, sortBy } from 'lodash'

export default {
  computed: {
    hasOptions() {
      return get(this.product, 'has_options', false)
    },
    attributes() {
      let attributes = this.product.attributes || []

      if (attributes.length) {
        attributes = attributes.filter(
          (a) => a.has_variations && a.terms && a.terms.length
        )

        // Sort by ID
        return attributes.map((a) => {
          a.terms = sortBy(a.terms, 'id')

          return a
        })
      }

      return []
    },
    priceRange() {
      const {
        price_range: { max_amount: maxAmount = '', min_amount: minAmount = '' },
      } = this.prices

      return {
        minAmount,
        maxAmount,
      }
    },
    minPrice() {
      const { minAmount } = this.priceRange

      return this.toPriceFormat(minAmount)
    },
    maxPrice() {
      const { maxAmount } = this.priceRange

      return this.toPriceFormat(maxAmount)
    },
    fullPrice() {
      let price

      if (this.minPrice !== this.maxPrice) {
        price = `${this.currencySymbol}${this.minPrice} - ${this.currencySymbol}${this.maxPrice}`
      } else {
        price = `${this.currencySymbol}${this.price}`

        price =
          this.onSale && this.salePrice
            ? `${price} <del>${this.currencySymbol}${this.regularPrice}</del>`
            : price
      }

      return price
    },
  },
}
