import get from 'lodash/get'
import { usePriceFormat } from '~/composables/usePriceFormat'

const { priceFormat } = usePriceFormat()

export default {
  props: {
    product: {
      type: Object,
      default() {
        return {}
      },
    },
  },
  computed: {
    name() {
      return this.product.name
    },
    productImage() {
      return get(this.product, 'images.0.src', '')
    },
    onSale() {
      return this.product.on_sale
    },
    prices() {
      return get(this.product, 'prices', {})
    },
    regularPrice() {
      const { regular_price: regularPrice = '' } = this.prices

      return this.toPriceFormat(regularPrice)
    },
    salePrice() {
      const { sale_price: salePrice = '' } = this.prices

      return this.toPriceFormat(salePrice)
    },
    price() {
      const { price = '' } = this.prices

      return this.toPriceFormat(price)
    },
    currencySymbol() {
      const { currency_symbol: currencySymbol } = this.prices

      return currencySymbol
    },
    currencyMinorUnit() {
      const { currency_minor_unit: currencyMinorUnit } = this.prices

      return currencyMinorUnit
    },
    fullPrice() {
      let price = `${this.currencySymbol}${this.price}`

      price =
        this.onSale && this.salePrice
          ? `${price} <del>${this.currencySymbol}${this.regularPrice}</del>`
          : price

      return price
    },
    parentProduct() {
      return this.product.parent_product || null
    },
    slug() {
      const permalink = get(this.product, 'permalink', '')

      return permalink.split('/').filter(Boolean).pop()
    },
    link() {
      let slug = this.slug

      if (this.parentProduct) {
        slug = this.parentProduct.slug
      }

      return `/product/${slug}`
    },
    description() {
      return this.product.description
    },
    shortDescription() {
      return this.product.short_description
    },
    sku() {
      return this.product.sku
    },
    categories() {
      return this.product.categories
    },
    stock() {
      return this.product.stock_quantity || 99
    },
    inStock() {
      return this.product.is_in_stock
    },
    soldIndividually() {
      return this.product.sold_individually || false
    },
  },
  methods: {
    toPriceFormat(price) {
      return priceFormat(price, this.currencyMinorUnit)
    },
  },
}
