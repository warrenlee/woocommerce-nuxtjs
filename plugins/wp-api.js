// eslint-disable-next-line no-unused-vars
import addOAuthInterceptor from 'axios-oauth-1.0a'

const HEADER_NONCE_KEY = 'X-WC-Store-API-Nonce'

export default ({ $axios, $config, store }, inject) => {
  const {
    browserBaseURL,
    perPage,
    woocommerceKey: key,
    woocommerceSecret: secret,
  } = $config

  // dev only
  // process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0

  const axios = $axios.create({
    baseURL: browserBaseURL,
    headers: {
      'Content-Type': 'application/json',
      [HEADER_NONCE_KEY]: store.state.nonce.token || '',
    },
    params: {
      _locale: 'user',
    },
  })

  addOAuthInterceptor(axios, {
    // OAuth consumer key and secret
    key,
    secret,

    // HMAC-SHA1 and HMAC-SHA256 are supported
    algorithm: 'HMAC-SHA1',
  })

  const updateNonce = (headers) => {
    const { [HEADER_NONCE_KEY.toLowerCase()]: nonce } = headers

    store.commit('nonce/UPDATE_NONCE', nonce)
  }

  const getProducts = async (page = 1, per_page = perPage, params = {}) => {
    return await axios
      .get('/wp-json/wc/store/products', {
        params: {
          page,
          per_page,
          order: 'desc',
          orderby: 'date',
          ...params,
        },
      })
      .then(({ data: products, headers }) => {
        const { 'x-wp-total': total, 'x-wp-totalpages': totalPages } = headers

        return {
          products,
          total: parseInt(total),
          totalPages: parseInt(totalPages),
        }
      })
      .catch((e) => e)
  }

  const getProductBySlug = async (slug) => {
    return await axios
      // find product by slug from wp
      .get('/wp-json/wp/v2/product', {
        params: {
          slug,
          per_page: 1,
        },
      })
      .then(async ({ data, headers }) => {
        // find product by ID from store
        const { id } = data.shift()

        return await axios
          .get(`/wp-json/wc/store/products/${id}`)
          .then(({ data }) => data)
      })
  }

  const getProductById = async (id) => {
    return await axios
      .get(`/wp-json/wc/store/products/${id}`)
      .then(({ data }) => data)
  }

  const getCategories = async () => {
    return await axios
      .get('/wp-json/wc/store/products/categories')
      .then(({ data }) => data)
      .catch((e) => e)
  }

  const addCartItem = async (id, quantity, variation) => {
    return await axios
      .post('/wp-json/wc/store/cart/add-item', {
        id,
        quantity,
        variation,
      })
      .then(({ data, headers }) => {
        updateNonce(headers)

        // Add response to store
        store.commit('cart/UPDATE_CART', data)

        return data
      })
  }

  const removeCartItem = async (key) => {
    return await axios
      .post('/wp-json/wc/store/cart/remove-item', {
        key,
      })
      .then(({ data, headers }) => {
        updateNonce(headers)

        // Add response to store
        store.commit('cart/UPDATE_CART', data)

        return data
      })
  }

  const clearCart = async () => {
    return await axios
      .delete('/wp-json/wc/store/cart/items')
      .then(({ data, headers }) => {
        updateNonce(headers)

        // Add response to store
        store.commit('cart/UPDATE_CART', data)

        return data
      })
  }

  const getCart = async () => {
    return await axios
      .get('/wp-json/wc/store/cart')
      .then(({ data, headers }) => {
        updateNonce(headers)

        return data
      })
  }

  const updateCustomer = async (payload) => {
    return await axios
      .post('/wp-json/wc/store/cart/update-customer', {
        ...payload,
      })
      .then(({ data, headers }) => {
        updateNonce(headers)

        return data
      })
  }

  const selectShippingRate = async (packageId, rateId) => {
    return await axios
      .post('/wp-json/wc/store/cart/select-shipping-rate', {
        package_id: packageId,
        rate_id: rateId,
      })
      .then(({ data, headers }) => {
        updateNonce(headers)

        // Add response to store
        store.commit('cart/UPDATE_CART', data)

        return data
      })
  }

  const checkout = async (payload) => {
    return await axios
      .post('/wp-json/wc/store/checkout', {
        ...payload,
      })
      .then(({ data, headers }) => {
        updateNonce(headers)

        return data
      })
  }

  inject('api', {
    products(page = 1, per_page = perPage, params = {}) {
      return getProducts(page, per_page, params)
    },
    product(slug) {
      return getProductBySlug(slug)
    },
    productId(id) {
      return getProductById(id)
    },
    categories() {
      return getCategories()
    },
    addCartItem(id, quantity, variation) {
      return addCartItem(id, quantity, variation)
    },
    cart() {
      return getCart()
    },
    shippingRate(packageId, rateId) {
      return selectShippingRate(packageId, rateId)
    },
    updateCustomer,
    removeCartItem,
    clearCart,
    checkout,
  })
}
