# woocommerce-nuxtjs

## Before Setup

Create a `.env` file

```dotenv
BASE_URL=http://localhost:8080 # URL to wordpress
WOOCOMMERCE_KEY=ck_key # API key
WOOCOMMERCE_SECRET=cs_secret # API secret
```

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
